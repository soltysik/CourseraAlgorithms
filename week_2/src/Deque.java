import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * Mateusz Sołtysik
 * Copyright, 2014.
 */
public class Deque<Item> implements Iterable<Item> {
    private Node first;
    private Node last;

    // construct an empty deque
    public Deque() {
        first = null;
        last = null;
    }

    // is the deque empty?
    public boolean isEmpty() {
        return (first == null);
    }

    // return the number of items on the deque
    public int size() {
        if (isEmpty()) return 0;
        int count = 1;
        Node node = first;
        while (node != last) {
            count++;
            node = node.next;
        }

        return count;
    }

    // insert the item at the front
    public void addFirst(Item item) {
        if (item == null) throw new NullPointerException();
        Node node = new Node(item);
        if (first != null) {
            node.next = first;
            first.prev = node;
            node.prev = null;
            first = node;
        } else {
            first = node;
            last = node;
            node.prev = null;
            node.next = null;
        }
    }

    // insert the item at the end
    public void addLast(Item item) {
        if (item == null) throw new NullPointerException();
        Node node = new Node(item);
        if (last != null) {
            node.prev = last;
            last.next = node;
            node.next = null;
            last = node;
        } else {
            first = node;
            last = node;
            node.prev = null;
            node.next = null;
        }
    }

    // delete and return the item at the front
    public Item removeFirst() {
        if (isEmpty()) throw new NoSuchElementException();
        Item item = first.item;
        first = first.next;
        if (first != null) {
            first.prev = null;
        } else {
            last = null;
        }
        return item;
    }

    // delete and return the item at the end
    public Item removeLast() {
        if (isEmpty()) throw new NoSuchElementException();
        Item item = last.item;
        last = last.prev;
        if (last != null) {
            last.next = null;
        } else {
            first = null;
        }

        return item;
    }

    // return an iterator over items in order from front to end
    public Iterator<Item> iterator() {
        return new Iterator<Item>() {
            private Node current = first;

            public boolean hasNext() {
                return current != null;
            }

            public Item next() {
                if (current == null) throw new NoSuchElementException();
                Item item = current.item;
                current = current.next;
                return item;
            }

            public void remove() {
                throw new UnsupportedOperationException();
            }
        };
    }


    private class Node {
        private Item item;
        private Node next;
        private Node prev;

        public Node(Item item) {
            this.item = item;
        }
    }
}
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.Iterator;

import static junit.framework.TestCase.assertFalse;
import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;

/**
 * Mateusz Sołtysik
 * Copyright, 2014.
 */
public class DequeTest {
    private Deque<String> deque;

    @Before
    public void setUp() throws Exception {
        deque = new Deque<String>();

    }

    @After
    public void tearUp() throws Exception {
        deque = null;
    }

    @Test
    public void testIsEmpty() throws Exception {
        assertTrue(deque.isEmpty());

        deque.addFirst("1'st element");
        assertFalse(deque.isEmpty());
    }
    @Test
    public void testSize() throws Exception {
        assertEquals(0, deque.size());

        deque.addFirst("1'st element");
        assertEquals(1, deque.size());

        deque.addFirst("2'nd element");
        assertEquals(2, deque.size());

        deque.addFirst("3'rd element");
        assertEquals(3, deque.size());

        deque.removeLast();
        assertEquals(2, deque.size());
    }

    @Test
    public void testAddFirst() throws Exception {
        deque.addFirst("2'nd element");
        deque.addFirst("1'st element");

        Iterator<String> iterator = deque.iterator();
        assertEquals("1'st element", iterator.next());
        assertEquals("2'nd element", iterator.next());
    }

    @Test
    public void testAddLast() throws Exception {
        deque.addLast("1'st element");
        deque.addLast("2'nd element");

        Iterator<String> iterator = deque.iterator();
        assertEquals("1'st element", iterator.next());
        assertEquals("2'nd element", iterator.next());
    }

    @Test
    public void testRemoveFirst() throws Exception {
        deque.addFirst("2'nd element");
        deque.addFirst("1'st element");

        assertEquals("1'st element", deque.removeFirst());

    }

    @Test
    public void testRemoveLast() throws Exception {
        deque.addLast("1'st element");
        deque.addLast("2'nd element");

        assertEquals("2'nd element", deque.removeLast());

    }
}

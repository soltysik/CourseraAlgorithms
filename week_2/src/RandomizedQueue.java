import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * Mateusz Sołtysik,
 * Copyright, 2014.
 */
public class RandomizedQueue<Item> implements Iterable<Item> {
    private Item[] queue;
    private int N;
    private int capacity;

    // construct an empty randomized queue
    public RandomizedQueue() {
        N = 0;
        capacity = 1;
        queue = (Item[]) new Object[capacity];
    }


    // is the queue empty?
    public boolean isEmpty() {
        return N == 0;
    }

    // return the number of items on the queue
    public int size() {
        return N;
    }

    // add the item
    public void enqueue(Item item) {
        if (item == null) throw new NullPointerException();
        if (N + 1 > capacity) {
            resizePlus();
        }
        queue[N++] = item;
    }

    // doubly increase size of queue
    private void resizePlus() {
        capacity *= 2;
        Item[] newQueue = (Item[]) new Object[capacity];
        int index = 0;
        for (Item i : queue) {
            newQueue[index++] = i;
        }
        queue = newQueue;
    }

    // doubly decrease size of queue
    private void resizeMinus() {
        capacity /= 2;
        Item[] newQueue = (Item[]) new Object[capacity];
        int index = 0;
        for (int i = 0; i < capacity; i++) {
            newQueue[index++] = queue[i];
        }
        queue = newQueue;
    }


    // delete and return a random item
    public Item dequeue() {
        if (isEmpty()) throw new NoSuchElementException();

        int i = StdRandom.uniform(N);
        Item ret = queue[i];
        queue[i] = queue[--N];
        queue[N] = null;

        if (capacity / 4 > N) {
            resizeMinus();
        }

        return ret;

    }

    // return (but do not delete) a random item
    public Item sample() {
        if (isEmpty()) throw new NoSuchElementException();
        int index = StdRandom.uniform(size());
        return queue[index];
    }

    // return an independent iterator over items in random order
    public Iterator<Item> iterator() {

        return new MyIterator();
    }

    private class MyIterator implements Iterator {

        private int current = 0;
        private int[] shuffledIndexes = new int[N];

        public boolean hasNext() {
            if (current == 0) {
                shuffle();
            }
            return current < N;
        }

        public Item next() {
            if (current == 0) {
                shuffle();
            }
            if (current >= N || size() == 0) {
                throw new NoSuchElementException();
            }
            return queue[shuffledIndexes[current++]];
        }

        private void shuffle() {
            for (int i = 0; i < N; i++) {
                shuffledIndexes[i] = i;
            }
            StdRandom.shuffle(shuffledIndexes);
        }

        public void remove() {
            throw new UnsupportedOperationException();
        }
    }

}

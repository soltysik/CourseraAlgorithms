# Algorithms Part 1

### Programming Assignment 1: Percolation
[Specification](http://coursera.cs.princeton.edu/algs4/assignments/percolation.html)

[Checklist](http://coursera.cs.princeton.edu/algs4/checklists/percolation.html)

[Assessment Summary](https://github.com/msoltysik/Algorithms-Part-1/blob/master/week_1/src/assessment_summary.txt)
### Programming Assignment 2: Randomized Queues and Deques
[Specification](http://coursera.cs.princeton.edu/algs4/assignments/queues.html)

[Checklist](http://coursera.cs.princeton.edu/algs4/checklists/queues.html)

[Assessment Summary](https://github.com/msoltysik/Algorithms-Part-1/blob/master/week_2/src/assessment_summary.txt)

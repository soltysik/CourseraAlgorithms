public class PercolationStats {
    private double[] results;
    private int T;

    // perform T independent computational experiments on an N-by-N grid
    public PercolationStats(int N, int T) {
        if (N <= 0 || T <= 0) throw new IllegalArgumentException();
        Percolation percolation;
        this.T = T;
        results = new double[T];
        for (int testNumber = 0; testNumber < T; testNumber++) {
            percolation = new Percolation(N);
            int result = 0;
            while (!percolation.percolates()) {
                int i = StdRandom.uniform(1, N + 1);
                int j = StdRandom.uniform(1, N + 1);
                if (!percolation.isOpen(i, j)) {
                    percolation.open(i, j);
                    result++;
                }
            }
            double fraction = (double) result / (N * N);
            results[testNumber] = fraction;
        }

    }

    // test client, described below
    public static void main(String[] args) {
        int N = Integer.parseInt(args[0]);
        int T = Integer.parseInt(args[1]);
        PercolationStats percolationStats = new PercolationStats(N, T);

        StdOut.println("mean                    = "
                + percolationStats.mean());
        StdOut.println("stddev                  = "
                + percolationStats.stddev());
        StdOut.println("95% confidence interval = "
                + percolationStats.confidenceLo()
                + ", " + percolationStats.confidenceHi());
    }

    // sample mean of percolation threshold
    public double mean() {
        return StdStats.mean(results);
    }

    // sample standard deviation of percolation threshold
    public double stddev() {
        return StdStats.stddev(results);
    }

    // returns lower bound of the 95% confidence interval
    public double confidenceLo() {
        return mean() - ((1.96 * stddev()) / Math.sqrt(T));
    }

    // returns upper bound of the 95% confidence interval
    public double confidenceHi() {
        return mean() + ((1.96 * stddev()) / Math.sqrt(T));
    }
}
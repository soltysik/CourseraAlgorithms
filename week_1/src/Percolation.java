public class Percolation {
    private final int N;
    private WeightedQuickUnionUF weightedQuickUnionUF;
    private boolean[][] opened;

    // create N-by-N grid, with all sites blocked
    public Percolation(int N) {
        if (N <= 0) throw new IllegalArgumentException();
        this.N = N;
        this.weightedQuickUnionUF = new WeightedQuickUnionUF(N * N + 2);
        this.opened = new boolean[N][N];

        for (int i = 1; i < N; i++) {
            weightedQuickUnionUF.union(0, i);
            weightedQuickUnionUF.union(N-1, N- i - 1);
        }
    }

    // convert (i, j) to cords
    private int cords(int i, int j) {
        return (i - 1) * N + j;
    }

    // open site (row i, column j) if it is not already
    public void open(int i, int j) {
        opened[i - 1][j - 1] = true;
        if (i == 1) {
            weightedQuickUnionUF.union(cords(i, j), 0);
        }
        if (i == N) {
            weightedQuickUnionUF.union(cords(i, j), N * N + 1);
        }

        if (j > 1 && isOpen(i, j - 1)) {
            weightedQuickUnionUF.union(cords(i, j), cords(i, j - 1));
        }
        if (j < N && isOpen(i, j + 1)) {
            weightedQuickUnionUF.union(cords(i, j), cords(i, j + 1));
        }
        if (i > 1 && isOpen(i - 1, j)) {
            weightedQuickUnionUF.union(cords(i, j), cords(i - 1, j));
        }
        if (i < N && isOpen(i + 1, j)) {
            weightedQuickUnionUF.union(cords(i, j), cords(i + 1, j));
        }
    }

    // is site (row i, column j) open?
    public boolean isOpen(int i, int j) {
        return opened[i - 1][j - 1];
    }

    // is site (row i, column j) full?
    public boolean isFull(int i, int j) {
        if (isInvalid(i, j)) throw new IndexOutOfBoundsException();
        return  isOpen(i, j) && weightedQuickUnionUF.connected(0, cords(i, j));
    }

    // does the system percolate?
    public boolean percolates() {
        return weightedQuickUnionUF.connected(0, N * N + 1);
    }

    // true, if 0 < i < N and 0 < j < N
    private boolean isValid(int i, int j) {
        return !isInvalid(i, j);
    }

    // false, if 0 < i < N and 0 < j < N
    private boolean isInvalid(int i, int j) {
        return isInvalid(i) || isInvalid(j);
    }

    // false, if 0 < i < N
    private boolean isInvalid(int i) {
        return i <= 0 || i > N;
    }
}
